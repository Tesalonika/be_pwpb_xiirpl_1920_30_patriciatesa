<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mlogin');
        $this->load->model('mdata');
        $this->load->model('mtrs');
        $this->load->model('mord');
        $this->load->model('medt');
    }
    public function index()
    {
        $this->load->view('login');
    }
    public function aksi_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
            'username' => $username,
            'password' => $password
        );
        $cek = $this->mlogin->cek_login("user", $where)->row_array();
        // var_dump($cek);
        // die;
        if ($cek > 0) {
            if ($cek['id_level'] == '5') {
                $data_session = array(
                    'username' => $username,
                    'password' => $password
                );

                $this->session->set_userdata($data_session);
                echo "Berhasil";

                redirect("beranda/beranda");
            } else if ($cek['id_level'] == '1') {
                redirect('beranda/admin');
            } else if ($cek['id_level'] == '2') {
                redirect('beranda/waiter');
            } else if ($cek['id_level'] == '3') {
                redirect('beranda/kasir');
            } else if ($cek['id_level'] == '4') {
                redirect('beranda/owner');
            }
        } else {
            echo "Username dan password salah !";
        }
    }

    public function daftar()
    {
        $this->load->view('pelanggan/daftar');
    }
    public function prosesdaftar()
    {
        $id_user = $this->input->post('id_user');
        $id_level = $this->input->post('id_level');
        //$username = $this->input->post('m');
        $password = $this->input->post('pwd');
        $nama_user = $this->input->post('nama_user');
        $alamat = $this->input->post('alamat');
        //$where = array('id_user' => $id_user);
        $data = array(
            'id_user' => $id_user,
            'id_level' => $id_level,
            'username' => $nama_user,
            'password' => $password,
            'nama_user' => $nama_user,
            'alamat' => $alamat,
        );

        $this->mlogin->input_user($data);
        redirect('beranda/index');
    }
    public function beranda()
    {
        $this->load->view('pelanggan/beranda');
    }
    public function masakan()
    {
        $data['judul'] = 'List Makanan';
        $data['masakan'] = $this->mdata->masakan()->result();
        $this->load->view('pelanggan/menu', $data);
    }
    public function transaksi()
    {
        $data['judul'] = 'Transaksi';
        $data['transaksi'] = $this->mtrs->transaksi()->result();
        $this->load->view('pelanggan/transaksi', $data);
    }
    public function detailorder()
    {
        $data['judul'] = 'Detail Order';
        $where = array('id_detail_order');
        $data['detail_order'] = $this->mord->detail_order($where)->row_array();
        $this->load->view('pelanggan/detailorder', $data);
    }
    public function edit()
    {
        $data['judul'] = 'Edit';
        $data['edit'] = $this->medt->edit()->result();
        $this->load->view('pelanggan/edit', $data);
    }
    // public function hapus()
    // {
    //     $proses = $this->mhapus->hapus($no);
    //     if (!$proses) {
    //         redirect(base_url('beranda/masakan'));
    //     } else {
    //         echo "Data Gagal dihapus";
    //     }
    // }

    public function admin()
    {

        $this->load->view('admin/dashboard');
    }

    public function owner()
    {

        $this->load->view('admin/dashboard');
    }
    public function waiter()
    {

        $this->load->view('admin/dashboard');
    }
    public function laporan()
    {

        $this->load->view('admin/laporan');
    }
    public function makanan()
    {
        $data['judul'] = 'List Makanan';
        $data['makanan'] = $this->mdata->makanan()->result();
        $this->load->view('admin/masakan', $data);
    }
    function hapus_data($id_masakan)
    {
        $where = array('id_masakan' => $id_masakan);
        $this->mdata->delete($where, 'masakan');
        redirect('beranda/makanan');
    }
    function edit_data($id_masakan)
    {
        $where = array('id_masakan' => $id_masakan);

        $data['masakan'] = $this->mdata->edit_data($where, 'masakan')->row();
        $this->load->view('admin/edit', $data);
    }
    public function prosesedit()
    {
        $id_masakan = $this->input->post('id_masakan');
        $nama_masakan = $this->input->post('nama');
        $harga_masakan = $this->input->post('harga');
        $status_masakan = $this->input->post('status');
        $where = array('id_masakan' => $id_masakan);
        $data = array(
            'nama_masakan' => $nama_masakan,
            'harga' => $harga_masakan,
            'status_masakan' => $status_masakan,
        );
        $this->mdata->proses_edit($data, $where);
        redirect('beranda/makanan');
    }
    function tambahdata()
    {
        // $where = array('id_masakan' => $id_masakan);
        // $data['masakan'] = $this->mdata->tambah_data($where, 'masakan')->row();
        $this->load->view('admin/tambah');
    }
    public function prosestambah()
    {
        $id_masakan = $this->input->post('id_masakan');
        $nama_masakan = $this->input->post('nama');
        $harga_masakan = $this->input->post('harga');
        $status_masakan = $this->input->post('status');
        $where = array('id_masakan' => $id_masakan);
        $data = array(
            // 'id_masakan' => $id_masakan,
            'nama_masakan' => $nama_masakan,
            'harga' => $harga_masakan,
            'status_masakan' => $status_masakan,
        );
        $this->mdata->tambah_masakan($data, 'masakan');
        redirect('beranda/makanan');
    }
    public function user()
    {
        $data['judul'] = 'List User';
        $data['user'] = $this->mdata->user()->result();
        $this->load->view('admin/user', $data);
    }
    function delete_data($id_user)
    {
        $where = array('id_user' => $id_user);
        $this->mdata->delete_data($where, 'user');
        redirect('beranda/user');
    }
    function edituser($id_user)
    {
        $where = array('id_user' => $id_user);
        $data['user'] = $this->mdata->edit_user($where, 'user')->row();
        $this->load->view('admin/edituser', $data);
    }
    public function prosesedituser()
    {
        $id_user = $this->input->post('id_user');
        $id_level = $this->input->post('id_level');
        $username = $this->input->post('username');
        $password = $this->input->post('pwd');
        $nama_user = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $where = array('id_user' => $id_user);
        $data = array(
            'username' => $username,
            'nama_user' => $nama_user,
            'alamat' => $alamat,
        );
        // var_dump($data);
        // die;
        $this->mdata->proses_edit_user($data, $where);
        redirect('beranda/user');
    }
    public function detailuser($id)
    {
        $data['judul'] = 'Detail User';
        $where = array('id_user' => $id);
        $data['detail_user'] = $this->mdata->detail_user($where)->row_array();
        $this->load->view('admin/detailuser', $data);
    }
    function bayardata()
    {
        $data['judul'] = 'Transaksi';
        $data['bayar_data'] = $this->mdata->bayar_data()->result();
        $this->load->view('admin/transaksi', $data);
    }
    function hapusbayar($id_transaksi)
    {
        $where = array('id_transaksi' => $id_transaksi);
        $this->mdata->hapus_bayar($where, 'transaksi');
        redirect('beranda/bayardata');
    }
    public function pesanan()
    {
        $data['judul'] = 'List Pesanan';
        $data['pesanan'] = $this->mdata->pesanan()->result();
        $this->load->view('admin/pesanan', $data);
    }
    public function masuk()
    {
        $data['judul'] = 'Pemasukan';
        $data['laporan'] = $this->mdata->masuk()->result();
        $this->load->view('admin/laporanmasuk', $data);
    }
    public function keluar()
    {
        $data['judul'] = 'Pengeluaran';
        $data['laporan'] = $this->mdata->keluar()->result();
        $this->load->view('admin/laporankeluar', $data);
    }
}
