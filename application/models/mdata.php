<?php

class mdata extends CI_Model
{
    function masakan()
    {
        return $this->db->get('masakan');
    }

    function input_data($data, $table)
    {
        $this->db->insert($table, $data);
    }
    function makanan()
    {
        return $this->db->get('masakan');
    }
    function delete($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    function edit_data($where, $table)
    {
        //$this->db->where($where);
        return $this->db->get_where($table, $where);
    }
    function tambah_data($data, $table)
    {
        //$this->db->where($where);
        return $this->db->get_where($table, $data);
    }
    function proses_edit($data, $where)
    {
        $this->db->where($where);
        $this->db->update('masakan', $data);
    }
    function tambah_masakan($data, $table)
    {
        $this->db->insert($table, $data);
    }
    function proses_tambah($data, $where)
    {
        $this->db->where($where);
        $this->db->create('masakan', $data);
    }

    function user()
    {
        return $this->db->get('user');
    }
    function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    function edit_user($where, $table)
    {
        //$this->db->where($where);
        return $this->db->get_where($table, $where);
    }
    function proses_edit_user($data, $where)
    {
        $this->db->where($where);
        $this->db->update('user', $data);
    }
    function detail_user($where)
    {
        return $this->db->get_where('user', $where);
    }
    // function bayar($where, $table)
    // {
    //     //$this->db->where($where);
    //     return $this->db->get_where($table, $where);
    // }
    function bayar_data()
    {
        //$this->db->where($where);
        return $this->db->get('transaksi');
    }
    function hapus_bayar($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    function pesanan()
    {
        return $this->db->get('t_order');
    }
    function masuk()
    {
        return $this->db->get('transaksi');
    }
    function keluar()
    {
        return $this->db->get('transaksi');
    }
}
