<!DOCTYPE html>
<html lang="en">

<head>
	<title>LOGIN</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="../admin/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body background="<?= base_url('assets/img/3.jpg'); ?>">

	<div class="container" style="padding-top: 200px;">
		<div class="circle">
			<div class="login">
				<h2 style="color:white;">RESTAURANTS CILPIT</h2>
				<h3 style="color:white;"> LOGIN </h3>
				<form action="<?= base_url('Beranda/aksi_login'); ?>" method="POST">
					<p style="color:white;">Welcome To Restaurant CILPIT</p>
					<div class="form-group">
						<input type="text" class="form-control" id="username" placeholder="Username" name="username">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="password" placeholder="Password" name="password">
					</div>
					<div class="form-group" style="color:red;">
						Belum punya akun? <a href="<?= base_url(); ?>daftar/index" style="color:red;">Register Disini</a>
					</div>
					<button type="submit" class="btn btn-danger">Submit</button>

				</form>
			</div>
		</div>
	</div>

</body>

</html>