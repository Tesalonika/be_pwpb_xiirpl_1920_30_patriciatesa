<!DOCTYPE html>
<html lang="en">

<head>
  <title>Beranda</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
  <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>

</head>

<body>

  <nav class="navbar navbar-inverse mynav">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="" style="padding: 0px;"><img src="<?= base_url('assets/img/1.jpg'); ?>" style="height: 100%;"></a>
      </div>

      <ul class="nav navbar-nav">
        <li><a href="#" style="color: black;" id="mybrand">
            <h5>RESTAURANTS CILPIT</h5>
          </a></li>
        <li><a href="<?= base_url('Beranda/beranda'); ?>" style="color: black;">Beranda</a></li>
        <li><a href="<?= base_url('beranda/masakan'); ?>" style="color: black;">Menu makanan</a></li>
        <li><a href="<?= base_url('Beranda/transaksi'); ?>" style="color: black;">Transaksi</a></li>
        <li><a href="<?= base_url('Beranda/index'); ?>" style="color: black;">Keluar</a></li>
      </ul>
    </div>


    <div class="mytransparent">
      <div class="container">
        <div>
          <h1 class="myh1">WELCOME RESTAURANTS CILPIT</h1>
        </div>
        <div class="row">
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/4.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Pizza</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/5.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">French Fries</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/9.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Juice</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/8.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Coffee</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/7.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Ramen</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/6.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Sushi</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/10.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Nasi Goreng</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/11.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Kwetiau</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/15.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Teh Manis</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/14.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Sop Buah</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/13.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Spaghetti</h5>
            </div>
          </div>
          <div class="col-sm-3">
            <img src="<?= base_url('assets/img/16.jpg'); ?>" style="width:100%;height:150px">
            <div class="card-body">
              <h5 class="card-title text-center" style="color: white; font-size: 24px">Udang</h5>
            </div>
          </div>
        </div>
      </div>

    </div>
  </nav>

  <footer class="text-center myfooter" style="background-color: red;">
    <div class="myfootertext"> CopyRight Tescil </div>
  </footer>

</body>

</html>