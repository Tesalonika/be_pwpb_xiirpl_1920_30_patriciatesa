<!DOCTYPE html>
<html>

<head>
    <title>Detail Order</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
</head>

<body>
    <nav class="navbar navbar-inverse mynav">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="" style="padding: 0px;"><img src="<?= base_url('assets/img/1.jpg'); ?>" style="height: 100%;"></a>
            </div>

            <ul class="nav navbar-nav">
                <li><a href="#" style="color: black;" id="mybrand">
                        <h5>RESTAURANTS CILPIT</h5>
                    </a></li>
                <li><a href="<?= base_url('Beranda/beranda'); ?>" style="color: black;">Beranda</a></li>
                <li><a href="<?= base_url('beranda/masakan'); ?>" style="color: black;">Menu makanan</a></li>
                <li><a href="<?= base_url('Beranda/transaksi'); ?>" style="color: black;">Transaksi</a></li>
                <li><a href="<?= base_url('Beranda/index'); ?>" style="color: black;">Keluar</a></li>
            </ul>
        </div>
    </nav>
    <br>
    <center>
        <h3>Detail Order</h3>
    </center>
    <br>
    <div class="mx-5">
        <form action="<?= base_url('Beranda/tambah_aksi'); ?>" method="POST" style="min-height:  100vh">
            <table class="table">
                <thead class="thead-dark" style="background-color:red">
                    <div class="content-wrapper">
                        <table class="table">
                            <tr>
                                <th>ID Detail Order</th>
                                <td><?php echo $detail_order['id_detail_order']; ?></td>
                            </tr>
                            <tr>
                                <th>ID Order</th>
                                <td><?php echo $detail_order['id_order']; ?></td>
                            </tr>
                            <tr>
                                <th>ID Masakan</th>
                                <td><?php echo $detail_order['id_masakan']; ?></td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td><?php echo $detail_order['keterangan']; ?></td>
                            </tr>
                        </table>
                        <!-- </div> -->
                        <div class="form-group">
                            <div class="col-sm-offset-10 col-sm-2">
                                <a href="<?php echo base_url('beranda/masakan') ?>"><button type="submit" class="btn btn-success">Kembali </button></a>
                            </div>
                        </div>
                    </div>
                    </tbody>
            </table>
        </form>
    </div>
    <footer class="text-center myfooter">
        <div class="myfootertext"> CopyRight Tescil </div>
    </footer>
</body>

</html>