<!DOCTYPE html>
<html>

<head>
    <title>Menu Makanan</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
</head>

<body>
    <nav class="navbar navbar-inverse mynav">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="" style="padding: 0px;"><img src="<?= base_url('assets/img/1.jpg'); ?>" style="height: 100%;"></a>
            </div>

            <ul class="nav navbar-nav">
                <li><a href="#" style="color: black;" id="mybrand">
                        <h5>RESTAURANTS CILPIT</h5>
                    </a></li>
                <li><a href="<?= base_url('Beranda/beranda'); ?>" style="color: black;">Beranda</a></li>
                <li><a href="<?= base_url('beranda/masakan'); ?>" style="color: black;">Menu makanan</a></li>
                <li><a href="<?= base_url('Beranda/transaksi'); ?>" style="color: black;">Transaksi</a></li>
                <li><a href="<?= base_url('Beranda/index'); ?>" style="color: black;">Keluar</a></li>
            </ul>
        </div>
    </nav>
    <br>
    <center>
        <h3>Menu Makanan</h3>
    </center>
    <br>
    <div class="mx-5">
        <form action="<?= base_url('Beranda/tambah_aksi'); ?>" method="POST" style="min-height:  100vh">
            <table class="table">
                <!-- <thead class="thead-dark" style="background-color:red"> -->
                <tr>
                    <th>No.</th>
                    <th scope="col">Nama Masakan</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Status Masakan</th>
                    <th scope="col">Keterangan</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($masakan as $msk) : ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $msk->nama_masakan ?></td>
                            <td><?php echo $msk->harga ?></td>
                            <td><?php echo $msk->status_masakan ?></td>
                            <td>
                                <a href="<?php echo base_url('beranda/detailorder/') ?>" class="btn btn-sm btn-success">Detail Order</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </form>
    </div>
    <footer class="text-center myfooter">
        <div class="myfootertext"> CopyRight Tescil </div>
    </footer>
</body>

</html>