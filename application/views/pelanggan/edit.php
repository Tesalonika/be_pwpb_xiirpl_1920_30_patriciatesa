<!DOCTYPE html>
<html>

<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('assets/css/jquery.min.js'); ?>"></script>
</head>

<body>

    <div class="container" style="margin-top: 80px">
        <div class="col-md-12">
            <form action="<?= base_url('beranda/edit'); ?>" method="POST" style="min-height:  100vh">
                <table style="margin:20px auto;">
                    <tbody>
                        <?php foreach ($edit as $edt) : ?>
                            <tr>
                                <td>Nama Masakan</td>
                                <td><?php echo $edt->nama_masakan ?></td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td><?php echo $edt->harga ?></td>
                            </tr>
                            <tr>
                                <td>Status Masakan</td>
                                <td><?php echo $edt->status_masakan ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </form>
            <button type="submit" class="btn btn-md btn-success">Batal</button>
            <button type="reset" class="btn btn-md btn-warning">Edit</button>
        </div>
    </div>
</body>

</html>